'use strict';

/**
 * Process a property that is held for sale
 * @param {net.biz.sciencePublicationNetwork.Publish} publicationInfo
 * @transaction
 */
async function publish(publicationInfo) {
    console.log('### publication ' + publicationInfo.toString());

    const registry = await getAssetRegistry('net.biz.sciencePublicationNetwork.PublicationAsset');
    const factory = getFactory();

    const publicationAsset = factory.newResource(
    	'net.biz.sciencePublicationNetwork',
    	'PublicationAsset',
    	publicationInfo.publicationId
    );
    publicationAsset.publication = publicationInfo.publication;

    await registry.add(publicationAsset);
}
